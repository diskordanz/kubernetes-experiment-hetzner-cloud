#!/bin/bash
set -euo pipefail

export CONTROL_PLANE_HOSTS=$(hcloud server list -l Project=private -l Role=control-plane -o noheader -o columns=ipv4 | sed 's/\(.*\)/"\1"/g' | tr '\n' ',' | sed 's/,$//g' )
export WORKER_HOSTS=$(hcloud server list -l Project=private -l Role=worker -o noheader -o columns=ipv4 | sed 's/\(.*\)/"\1"/g' | tr '\n' ',' | sed 's/,$//g' )
envsubst < inventory.json.tpl
