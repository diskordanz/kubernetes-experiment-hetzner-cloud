{
    "control_plane": {
        "hosts": [
            ${CONTROL_PLANE_HOSTS}
        ],
        "vars": {
          "ansible_python_interpreter": "/usr/bin/python3"
        }
    },
    "workers": {
        "hosts": [
            ${WORKER_HOSTS}
        ],
        "vars": {
          "ansible_python_interpreter": "/usr/bin/python3"
        }
    }
}
