# Super quick and dirty Kubernetes on Hetzner Cloud

Don't know about quick, but it sure is dirty :)

I used this setup to tinker around with setting up a very basic Kubernetes cluster on Hetzner Cloud, using kubeadm. The setup does not create a fully-fledged cluster, as
- the control plane is not initialized
- a network plugin like Calico is missing
- the worker nodes are not added to the cluster

However, with this setup as a basis, it is pretty simple to spin up a working cluster (without HA control plane). Note: this setup is **not** usable for production and should just be used for educational purposes.
