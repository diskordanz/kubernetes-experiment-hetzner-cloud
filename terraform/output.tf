output "public_ip_control_plane_nodes" {
  value = hcloud_server.control_plane[*].ipv4_address
}

output "public_ip_worker_nodes" {
  value = hcloud_server.worker[*].ipv4_address
}