variable "control_plane_count" {
  description = "Amount of control plane nodes"
  default = 1
}

variable "worker_count" {
  description = "Amount of worker nodes"
  default = 1
}