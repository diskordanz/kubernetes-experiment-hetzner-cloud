resource "hcloud_network" "internal" {
  name = "internal"
  ip_range = "10.0.0.0/8"

  labels = {
    Name = "internal"
    Project = "private"
  }
}
resource "hcloud_network_subnet" "internal_1" {
  network_id = hcloud_network.internal.id
  type = "cloud"
  network_zone = "eu-central"
  ip_range   = "10.0.1.0/24"
}

resource "hcloud_server_network" "control_plan" {
  server_id = hcloud_server.control_plane[count.index].id
  network_id = hcloud_network.internal.id
  count = length(hcloud_server.control_plane)
}

resource "hcloud_server_network" "worker" {
  server_id = hcloud_server.worker[count.index].id
  network_id = hcloud_network.internal.id
  count = length(hcloud_server.worker)
}