resource "hcloud_server" "control_plane" {
  name = "control-plane-${count.index}"
  image = "debian-10"
  server_type = "cx11"
  ssh_keys = ["id_hetzner"]
  location = var.hcloud_location
  user_data = templatefile("${path.module}/user_data.tpl", {username = var.ssh_user})

  labels = {
    Name = "control-plane-${count.index}"
    Role = "control-plane"
    Project = "private"
  }
  count = var.control_plane_count
}

resource "hcloud_server" "worker" {
  name = "worker-${count.index}"
  image = "debian-10"
  server_type = "cx11"
  ssh_keys = ["id_hetzner"]
  location = var.hcloud_location
  user_data = templatefile("${path.module}/user_data.tpl", {username = var.ssh_user})

  labels = {
    Name = "worker-${count.index}"
    Role = "worker"
    Project = "private"
  }
  count = var.worker_count
}